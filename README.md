# 百度统计的JavaApi

没办法，百度统计官方的JavaDemo不好使，只好自己编写，也顺便分享下。

目前没完全把文档上的接口都实现了，只是做了部分常用的作为例子，有需要的朋友请自行编写，大同小异了。

### 如何使用百度统计接口?

1. 百度统计接口 _不要钱_ ，网站日PV100即可在后台【管理】-> 【数据导出服务】里面开通。API每人每周的配额是2000，每周六会统一清零。（不过调用api几次后发现配额变成5w了，pv也还是100..目前未知原因）
2. 开通后在【数据导出服务】栏目中获得token。调用api时使用。
3. 使用方法请参见单元测试类，[me.kennylee.tongji.service.impl.ReportServiceImplTest](src/test/java/me/kennylee/tongji/service/impl/ReportServiceImplTest.java)

*注：使用单元测试时，请根据自己实际情况来填写 `config.properties` 文件。*

---------

附:

* [百度统计开放平台](https://tongji.baidu.com/open/api/more)
* [Tongji API开发文档](https://tongji.baidu.com/dataapi/file/TongjiApiFile.pdf)
* [百度统计官网](https://tongji.baidu.com/)



