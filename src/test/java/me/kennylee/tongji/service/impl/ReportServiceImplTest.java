package me.kennylee.tongji.service.impl;

import me.kennylee.tongji.Config;
import me.kennylee.tongji.ReportDataApiResponse;
import me.kennylee.tongji.ReportDataParam;
import me.kennylee.tongji.builder.bean.DistrictRpt;
import me.kennylee.tongji.builder.bean.TimeTrendRpt;
import me.kennylee.tongji.response.SiteInfo;
import me.kennylee.tongji.service.ReportService;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * <p> ReportService测试类 </p>
 * <p>Created on 4/11/2017.</p>
 *
 * @author kennylee
 */
public class ReportServiceImplTest {

    private ReportService reportService;

    private static Logger logger = LoggerFactory.getLogger(ReportServiceImplTest.class);

    private SiteInfo site = null;

    @Before
    public void init() throws ConfigurationException, IOException {
        Configurations configs = new Configurations();
        Configuration config = configs.properties(getClass().getClassLoader().getResource("config.properties"));
        String username = new String(config.getString("api.username").getBytes(PropertiesConfiguration.DEFAULT_ENCODING), "utf8");
        String password = config.getString("api.password");
        String token = config.getString("api.token");
        logger.debug("username: {}, passowrd: {}, token: {}", username, password, token);
        this.reportService = new ReportServiceImpl(new Config(username, password, token));
        this.site = reportService.getSiteList().get(0);
        logger.info(this.site.toString());
    }

    @Test
    public void getSiteList() throws Exception {
        List<SiteInfo> list = reportService.getSiteList();
        Assert.assertTrue(!list.isEmpty());
        for (SiteInfo info : list) {
            logger.info(info.toString());
        }
    }

    @Test
    public void getDistrictRpt() throws Exception {
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        ReportDataParam reportDataParam = ReportDataParam
                .newInstance(Long.valueOf(this.site.getSiteId()), cal.getTime(), now);
        ReportDataApiResponse<DistrictRpt> response = reportService.getDistrictRpt(reportDataParam);
        Assert.assertNotNull(response);
        Assert.assertTrue(!response.getItems().isEmpty());
        for (DistrictRpt o : response.getItems()) {
            System.out.println(o.toString());
        }
    }

    @Test
    public void getVisitDistrict() throws Exception {
        ReportDataApiResponse response = reportService.getVisitDistrict(getReportDataParam(), null);
        Assert.assertNotNull(response);
        System.out.println(response.getJson());
    }

    @Test
    public void getTimeTrendRpt() throws Exception {
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        ReportDataParam reportDataParam = ReportDataParam
                .newInstance(Long.valueOf(this.site.getSiteId()), cal.getTime(), cal.getTime());
        ReportDataApiResponse<TimeTrendRpt> response = reportService.getTimeTrendRpt(reportDataParam);
        Assert.assertNotNull(response);
        Assert.assertTrue(!response.getItems().isEmpty());
        for (TimeTrendRpt rpt : response.getItems()) {
            System.out.println(rpt);
        }

    }

    private ReportDataParam getReportDataParam() throws IOException {
        return ReportDataParam.newInstance(Long.valueOf(this.site.getSiteId()));
    }

}