package me.kennylee.tongji.service;

import me.kennylee.tongji.ReportDataApiResponse;
import me.kennylee.tongji.ReportDataParam;
import me.kennylee.tongji.builder.bean.DistrictRpt;
import me.kennylee.tongji.builder.bean.TimeTrendRpt;
import me.kennylee.tongji.response.SiteInfo;

import java.io.IOException;
import java.util.List;

/**
 * <p> 报表服务 </p>
 * <p>Created on 1/11/2017.</p>
 *
 * @author kennylee
 */
public interface ReportService {

    String TONG_JI_API_URL = "https://api.baidu.com/json/tongji/v1/ReportService";
    String GET_SITE_LIST_API_URL = TONG_JI_API_URL.concat("/getSiteList");
    String GET_DATA_API_URL = TONG_JI_API_URL.concat("/getData");

    List<SiteInfo> getSiteList() throws IOException;

    /**
     * 地域访问
     *
     * @param reportDataParam 基础参数
     * @param metrics         指标。可为null，则使用方法内有默认的指标。
     * @return
     */
    ReportDataApiResponse getVisitDistrict(ReportDataParam reportDataParam, String metrics);

    /**
     * 地域分布
     *
     * @param reportDataParam 基础参数
     * @return
     */
    ReportDataApiResponse<DistrictRpt> getDistrictRpt(ReportDataParam reportDataParam);

    /**
     * 趋势数据
     *
     * @param reportDataParam 基础参数
     * @return
     */
    ReportDataApiResponse<TimeTrendRpt> getTimeTrendRpt(ReportDataParam reportDataParam);
}
