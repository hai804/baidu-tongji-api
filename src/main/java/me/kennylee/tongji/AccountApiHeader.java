package me.kennylee.tongji;

/**
 * <p> 基础用户认证头部 </p>
 * <p>Created on 4/11/2017.</p>
 *
 * @author kennylee
 */
public class AccountApiHeader extends ApiHeader {

    public AccountApiHeader(String username, String password, String token) {
        super(1, password, token, username);
    }

    public static AccountApiHeader newInstance(Config config) {
        return new AccountApiHeader(config.getUsername(), config.getPassword(), config.getToken());
    }
}
