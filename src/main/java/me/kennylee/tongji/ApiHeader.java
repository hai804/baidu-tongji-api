package me.kennylee.tongji;

import com.google.gson.annotations.SerializedName;

/**
 * <p> 头部内容 </p>
 * <p>Created on 4/11/2017.</p>
 *
 * @author kennylee
 */
abstract class ApiHeader {
    @SerializedName("account_type")
    private int accountType;
    private String password;
    private String token;
    private String username;

    protected ApiHeader(int accountType, String password, String token, String username) {
        this.accountType = accountType;
        this.password = password;
        this.token = token;
        this.username = username;
    }

    public int getAccountType() {
        return accountType;
    }

    public void setAccountType(int accountType) {
        this.accountType = accountType;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
