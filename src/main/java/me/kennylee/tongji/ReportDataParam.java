package me.kennylee.tongji;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * <p> ReportData接口的基础参数 </p>
 * <p>Created on 5/11/2017.</p>
 *
 * @author kennylee
 */
public class ReportDataParam {
    private long siteId;
    private String beginDate;
    private String endDate;

    private ReportDataParam() {
        //do nothing
    }

    public static ReportDataParam newInstance(long siteId, Date begin, Date end) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        ReportDataParam parm = new ReportDataParam();
        parm.siteId = siteId;
        if (begin == null) {
            begin = weekBefore();
        }
        parm.beginDate = sdf.format(begin);
        if (end == null) {
            end = now();
        }
        parm.endDate = sdf.format(end);
        return parm;
    }

    /**
     * 日期没指定的话，默认是一周之前的时间为参数。
     *
     * @param siteId 网站id
     * @return
     */
    public static ReportDataParam newInstance(long siteId) {
        return newInstance(siteId, null, null);
    }


    public long getSiteId() {
        return siteId;
    }

    public void setSiteId(long siteId) {
        this.siteId = siteId;
    }

    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public static Date yesterday() {
        Calendar now = Calendar.getInstance();
        now.add(Calendar.DATE, -1);
        return now.getTime();
    }

    public static Date weekBefore() {
        Calendar now = Calendar.getInstance();
        now.add(Calendar.DATE, -7);
        return now.getTime();
    }

    public static Date now() {
        return Calendar.getInstance().getTime();
    }
}
