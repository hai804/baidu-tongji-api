package me.kennylee.tongji;

/**
 * <p> 配置 </p>
 * <p>Created on 4/11/2017.</p>
 *
 * @author kennylee
 */
public class Config {

    private String username;
    private String password;
    private String token;

    public Config(String username, String password, String token) {
        this.username = username;
        this.password = password;
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
