/**
 * <b>日期：</b>2017/11/05 4:09 PM<br/>
 * <b>Copyright (c)</b> 2017 广州天健软件有限公司<br/>
 */
package me.kennylee.tongji;

/**
 * <b>类名称：</b>ReportDataApiMethod<br/>
 * <b>类描述：</b> ReportData的method参数封装 <br/>
 * <b>备注：</b><br/>
 *
 * @author kennylee <br />
 * @version 1.0.0 <br/>
 */
public enum ReportDataApiMethod {
    VISIT_DISTRICT_A("visit/district/a"), OVERVIEW_DISTRICT_RPT("overview/getDistrictRpt"),
    OVERVIEW_TIME_TREND_RPT("overview/getTimeTrendRpt");
    private final String method;

    ReportDataApiMethod(String method) {
        this.method = method;
    }

    public String get() {
        return this.method;
    }
}
