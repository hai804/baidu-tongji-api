package me.kennylee.tongji;

import com.google.gson.Gson;

/**
 * <p> Api请求体 </p>
 * <p>Created on 4/11/2017.</p>
 *
 * @author kennylee
 */
public class ApiRequest<H extends ApiHeader, B extends ApiBody> {
    public H header;
    public B body;

    public ApiRequest(H header) {
        this.header = header;
    }

    public ApiRequest(H header, B body) {
        this.header = header;
        this.body = body;
    }

    public H getHeader() {
        return header;
    }

    public B getBody() {
        return body;
    }

    String toJSON() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
