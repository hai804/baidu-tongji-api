package me.kennylee.tongji.response;

/**
 * <p> 网站列表的回调信息 </p>
 * <p>Created on 5/11/2017.</p>
 *
 * @author kennylee
 */
public class SiteListApiResponse extends ApiResponse {
    private SiteInfoListData body;

    public SiteInfoListData getBody() {
        return body;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(super.toString()).append("\n");
        sb.append("SiteListApiResponse{");
        sb.append("body=").append(body);
        sb.append('}');
        return sb.toString();
    }
}
