package me.kennylee.tongji.response;

/**
 * <p> body信息 </p>
 * <p>Created on 5/11/2017.</p>
 *
 * @author kennylee
 */
public abstract class ApiResponse {
    private ApiResponseHeader header;

    public ApiResponseHeader getHeader() {
        return header;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ApiResponse{");
        sb.append("header=").append(header);
        sb.append('}');
        return sb.toString();
    }
}
