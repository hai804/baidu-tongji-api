package me.kennylee.tongji.response;

import com.google.gson.annotations.SerializedName;

/**
 * <p>  </p>
 * <p>Created on 5/11/2017.</p>
 *
 * @author kennylee
 */
public class SiteInfo {
    @SerializedName("status")
    private boolean isClose;
    private String domain;
    @SerializedName("site_id")
    private String siteId;

    public boolean isClose() {
        return isClose;
    }

    public String getDomain() {
        return domain;
    }

    public String getSiteId() {
        return siteId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SiteInfo{");
        sb.append("isClose=").append(isClose);
        sb.append(", domain='").append(domain).append('\'');
        sb.append(", siteId='").append(siteId).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
