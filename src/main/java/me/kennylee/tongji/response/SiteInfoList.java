package me.kennylee.tongji.response;

import java.util.List;

/**
 * <p> </p>
 * <p>Created on 5/11/2017.</p>
 *
 * @author kennylee
 */
public class SiteInfoList {
    private List<SiteInfo> list;

    public List<SiteInfo> getList() {
        return list;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SiteInfoList{");
        sb.append("list=").append(list);
        sb.append('}');
        return sb.toString();
    }
}
