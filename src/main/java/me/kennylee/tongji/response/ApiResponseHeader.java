package me.kennylee.tongji.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * <p> 头部信息 </p>
 * <p>Created on 5/11/2017.</p>
 *
 * @author kennylee
 */
public class ApiResponseHeader {
    private String desc;
    private List<String> failures;
    private int oprs;
    private int succ;
    private int oprtime;
    private int quota;
    private int rquota;
    @SerializedName("status")
    private boolean isClose;

    public String getDesc() {
        return desc;
    }

    public List<String> getFailures() {
        return failures;
    }

    public int getOprs() {
        return oprs;
    }

    public int getSucc() {
        return succ;
    }

    public int getOprtime() {
        return oprtime;
    }

    public int getQuota() {
        return quota;
    }

    public int getRquota() {
        return rquota;
    }

    public boolean isClose() {
        return isClose;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ApiResponseHeader{");
        sb.append("desc='").append(desc).append('\'');
        sb.append(", failures=").append(failures);
        sb.append(", oprs=").append(oprs);
        sb.append(", succ=").append(succ);
        sb.append(", oprtime=").append(oprtime);
        sb.append(", quota=").append(quota);
        sb.append(", rquota=").append(rquota);
        sb.append(", status=").append(isClose);
        sb.append('}');
        return sb.toString();
    }
}
