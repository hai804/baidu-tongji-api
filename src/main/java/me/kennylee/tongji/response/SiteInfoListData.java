package me.kennylee.tongji.response;

import java.util.List;

/**
 * <p> </p>
 * <p>Created on 5/11/2017.</p>
 *
 * @author kennylee
 */
public class SiteInfoListData {
    private List<SiteInfoList> data;

    public List<SiteInfoList> getData() {
        return data;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SiteInfoListData{");
        sb.append("data=").append(data);
        sb.append('}');
        return sb.toString();
    }
}
