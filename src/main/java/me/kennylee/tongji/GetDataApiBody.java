package me.kennylee.tongji;

import com.google.gson.annotations.SerializedName;

/**
 * <p> getData接口的请求body </p>
 * <p>Created on 4/11/2017.</p>
 *
 * @author kennylee
 */
public class GetDataApiBody extends ApiBody {

    /**
     * 站点 id
     */
    @SerializedName("site_id")
    private long siteId;
    /**
     * 通常对应要查询的报告
     */
    private String method;
    /**
     * 查询起始时间,例:20160501
     */
    @SerializedName("start_date")
    private String startDate;
    /**
     * 查询结束时间,例:20160531
     */
    @SerializedName("end_date")
    private String endDate;
    /**
     * 指标
     */
    private String metrics;

    /**
     * 对比查询起始时间
     */
    @SerializedName("start_date2")
    private String startDate2;
    /**
     * 对比查询结束时间
     */
    @SerializedName("end_date2")
    private String endDate2;
    /**
     * 时间粒度(只支持有该参数的报告): day/hour/week/month
     */
    private String gran;
    /**
     * 指标排序，示例:visitor_count,desc
     */
    private String order;
    /**
     * 获取数据偏移，用于分页;默认是 0
     */
    @SerializedName("start_index")
    private long startIndex;
    /**
     * 单次获取数据条数，用于分页;默认是 20; 0 表示获取所有数据.
     */
    @SerializedName("max_results")
    private int maxResults;

    private GetDataApiBody(Builder builder) {
        super();
        this.setSiteId(builder.siteId);
        this.setMethod(builder.method.get());
        this.setStartDate(builder.startDate);
        this.setEndDate(builder.endDate);
        this.setMetrics(builder.metrics);

        this.setStartDate2(builder.startDate2);
        this.setEndDate2(builder.endDate2);
        this.setGran(builder.gran);
        this.setOrder(builder.order);
        this.setStartIndex(builder.startIndex);
        this.setMaxResults(builder.maxResults);
    }

    public static class Builder {
        private final long siteId;
        private final ReportDataApiMethod method;
        private final String startDate;
        private final String endDate;
        private final String metrics;

        private String startDate2;
        private String endDate2;
        private String gran;
        private String order;
        private long startIndex;
        private int maxResults;

        public Builder(long siteId, ReportDataApiMethod method, String startDate, String endDate, String metrics) {
            this.siteId = siteId;
            this.method = method;
            this.startDate = startDate;
            this.endDate = endDate;
            this.metrics = metrics;
        }

        private boolean isSameDay() {
            return startDate.equals(endDate);
        }

        public Builder startDate2(String startDate2) {
            this.startDate2 = startDate2;
            return this;
        }

        public Builder endDate2(String endDate2) {
            this.endDate2 = endDate2;
            return this;
        }

        public Builder gran(String gran) {
            this.gran = gran;
            return this;
        }

        public Builder order(String order) {
            this.order = order;
            return this;
        }

        public Builder startIndex(long startIndex) {
            this.startIndex = startIndex;
            return this;
        }

        public Builder maxResults(int maxResults) {
            this.maxResults = maxResults;
            return this;
        }

        public GetDataApiBody build() {
            if (this.gran == null) {
                if (this.isSameDay()) {
                    this.gran = "hour";
                } else {
                    this.gran = "day";
                }
            }
            return new GetDataApiBody(this);
        }
    }

    public long getSiteId() {
        return siteId;
    }

    public void setSiteId(long siteId) {
        this.siteId = siteId;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getMetrics() {
        return metrics;
    }

    public void setMetrics(String metrics) {
        this.metrics = metrics;
    }

    public String getStartDate2() {
        return startDate2;
    }

    public void setStartDate2(String startDate2) {
        this.startDate2 = startDate2;
    }

    public String getEndDate2() {
        return endDate2;
    }

    public void setEndDate2(String endDate2) {
        this.endDate2 = endDate2;
    }

    public String getGran() {
        return gran;
    }

    public void setGran(String gran) {
        this.gran = gran;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public long getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(long startIndex) {
        this.startIndex = startIndex;
    }

    public int getMaxResults() {
        return maxResults;
    }

    public void setMaxResults(int maxResults) {
        this.maxResults = maxResults;
    }
}
