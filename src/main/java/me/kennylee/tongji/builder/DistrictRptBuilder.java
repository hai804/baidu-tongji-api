package me.kennylee.tongji.builder;

import me.kennylee.tongji.ReportDataItemBuilder;
import me.kennylee.tongji.builder.bean.DistrictRpt;

/**
 * <p> DistrictRpt的构造器 </p>
 * <p>Created on 5/11/2017.</p>
 *
 * @author kennylee
 */
public class DistrictRptBuilder extends ReportDataItemBuilder<DistrictRpt> {
    public DistrictRptBuilder(String json) {
        super(json);
    }
}
