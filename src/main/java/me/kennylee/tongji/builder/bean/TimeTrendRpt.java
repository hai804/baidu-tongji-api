package me.kennylee.tongji.builder.bean;

import com.google.gson.JsonArray;

/**
 * <p> 趋势 </p>
 * <p>Created on 6/11/2017.</p>
 *
 * @author kennylee
 */
public class TimeTrendRpt {

    private String date;
    /**
     * pv
     */
    private long pvCount;
    /**
     * uv
     */
    private long visitorCount;
    private long ipCount;
    /**
     * 跳出率
     */
    private float bounceRatio;
    /**
     * 平均访问时长,单位秒
     */
    private long avgVisitTime;

    public TimeTrendRpt(String date, long pvCount, long visitorCount, long ipCount, float bounceRatio, long avgVisitTime) {
        this.date = date;
        this.pvCount = pvCount;
        this.visitorCount = visitorCount;
        this.ipCount = ipCount;
        this.bounceRatio = bounceRatio;
        this.avgVisitTime = avgVisitTime;
    }

    public TimeTrendRpt(JsonArray dimensionElement, JsonArray indexElement) {
        super();
        long pvCount = 0L;
        long visitorCount = 0L;
        long ipCount = 0L;
        float bounceRatio = 0f;
        long avgVisitTime = 0;
        try {
            pvCount = indexElement.get(0).getAsLong();
        } catch (NumberFormatException e) {
            //do nothing
        }
        try {
            visitorCount = indexElement.get(1).getAsLong();
        } catch (NumberFormatException e) {
            //do nothing
        }
        try {
            ipCount = indexElement.get(2).getAsLong();
        } catch (NumberFormatException e) {
            //do nothing
        }
        try {
            bounceRatio = indexElement.get(3).getAsFloat();
        } catch (NumberFormatException e) {
            //do nothing
        }
        try {
            avgVisitTime = indexElement.get(4).getAsLong();
        } catch (NumberFormatException e) {
            //do nothing
        }
        this.date = dimensionElement.get(0).getAsString();
        this.pvCount = pvCount;
        this.visitorCount = visitorCount;
        this.ipCount = ipCount;
        this.bounceRatio = bounceRatio;
        this.avgVisitTime = avgVisitTime;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getPvCount() {
        return pvCount;
    }

    public void setPvCount(long pvCount) {
        this.pvCount = pvCount;
    }

    public long getVisitorCount() {
        return visitorCount;
    }

    public void setVisitorCount(long visitorCount) {
        this.visitorCount = visitorCount;
    }

    public long getIpCount() {
        return ipCount;
    }

    public void setIpCount(long ipCount) {
        this.ipCount = ipCount;
    }

    public float getBounceRatio() {
        return bounceRatio;
    }

    public void setBounceRatio(float bounceRatio) {
        this.bounceRatio = bounceRatio;
    }

    public long getAvgVisitTime() {
        return avgVisitTime;
    }

    public void setAvgVisitTime(long avgVisitTime) {
        this.avgVisitTime = avgVisitTime;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TimeTrendRpt{");
        sb.append("date='").append(date).append('\'');
        sb.append(", pvCount=").append(pvCount);
        sb.append(", visitorCount=").append(visitorCount);
        sb.append(", ipCount=").append(ipCount);
        sb.append(", bounceRatio='").append(bounceRatio).append('\'');
        sb.append(", avgVisitTime='").append(avgVisitTime).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
