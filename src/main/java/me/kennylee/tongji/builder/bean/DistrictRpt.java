package me.kennylee.tongji.builder.bean;

import com.google.gson.JsonArray;

/**
 * <p>地域分布</p>
 * <p>Created on 5/11/2017.</p>
 *
 * @author kennylee
 */
public class DistrictRpt {
    /**
     * 城市
     */
    private String name;
    private long pvCount;
    private float ratio;

    public DistrictRpt(String name, long pvCount, float ratio) {
        this.name = name;
        this.pvCount = pvCount;
        this.ratio = ratio;
    }

    public DistrictRpt(JsonArray dimensionElement, JsonArray indexElement) {
        super();
        long pvCount = 0;
        float ratio = 0F;
        this.name = dimensionElement.get(0).getAsString();
        try {
            pvCount = indexElement.get(0).getAsLong();
        } catch (NumberFormatException e) {
            //do nothing
        }
        try {
            ratio = indexElement.get(1).getAsFloat();
        } catch (NumberFormatException e) {
            //do nothing
        }
        this.pvCount = pvCount;
        this.ratio = ratio;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPvCount() {
        return pvCount;
    }

    public void setPvCount(long pvCount) {
        this.pvCount = pvCount;
    }

    public float getRatio() {
        return ratio;
    }

    public void setRatio(float ratio) {
        this.ratio = ratio;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DistrictRptItem{");
        sb.append("name='").append(name).append('\'');
        sb.append(", pvCount=").append(pvCount);
        sb.append(", ratio=").append(ratio);
        sb.append('}');
        return sb.toString();
    }
}
