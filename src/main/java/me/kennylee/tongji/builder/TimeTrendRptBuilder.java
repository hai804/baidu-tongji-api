package me.kennylee.tongji.builder;

import me.kennylee.tongji.ReportDataItemBuilder;
import me.kennylee.tongji.builder.bean.TimeTrendRpt;

/**
 * <p>Created on 6/11/2017.</p>
 *
 * @author kennylee
 */
public class TimeTrendRptBuilder extends ReportDataItemBuilder<TimeTrendRpt> {
    public TimeTrendRptBuilder(String json) {
        super(json);
    }
}
